package com.github.pedroluis02.jglhermitesplines;

import java.awt.BorderLayout;
import javax.swing.JFrame;

/**
 * MainFrame
 * @author Pedro Luis
 */
public class MainFrame extends JFrame {

    public MainFrame(String title) {
        super(title);
        ScenePanel scene = new ScenePanel();
        this.getContentPane().add(scene);
        this.getContentPane().add(BorderLayout.WEST, scene.showPanel());
        this.requestFocus();
    }
    
    public static void main(String[] args) {
        MainFrame frame = new MainFrame("Hermite Splines");
        frame.setSize(800, 700);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
    
}
