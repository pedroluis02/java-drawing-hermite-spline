package com.github.pedroluis02.jglhermitesplines;

/**
 * ScenePanel
 * @author Pedro Luis
 */
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.util.gl2.GLUT;

import javax.swing.JPanel;
import java.util.ArrayList;

public class ScenePanel extends GLCanvas implements GLEventListener {

    private GLU glu = new GLU();
    private GLUT glut = new GLUT();
    private Hermite hermite = new Hermite();
    private ArrayList <float[]> splines = new ArrayList<float[]>();
    private boolean activacion = false;

    // Variables declaration - do not modify
    private javax.swing.JButton Bañadir;
    private javax.swing.JButton Bclear;
    private javax.swing.JButton Bgraficar;
    private javax.swing.JTextArea TApu_pe;
    private javax.swing.JTextField TFpex;
    private javax.swing.JTextField TFpey;
    private javax.swing.JTextField TFpez;
    private javax.swing.JTextField TFpux;
    private javax.swing.JTextField TFpuy;
    private javax.swing.JTextField TFpuz;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration

    public ScenePanel() {
        this.addGLEventListener(this);
    }
 

    @Override
    public void init(GLAutoDrawable gldrawable) {
        final GL2 gl = gldrawable.getGL().getGL2();
        gl.glClearColor(0f, 0f, 0f, 0f); //color de fondo
    }

    @Override
    public void reshape(GLAutoDrawable gldrawable, int x, int y, int width, int height) {
        GL2 gl = gldrawable.getGL().getGL2();;
        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glLoadIdentity();
        gl.glOrtho(-5, 7, -7, 6, -10, 6);//projection
        glu.gluLookAt(-.9, .9, .9, -5.5, -3.5, -3.5, 0, 2.5, 0);//vista
        gl.glMatrixMode(GL2.GL_MODELVIEW);
        gl.glLoadIdentity();
        gl.glViewport(0, 0, width, height);
    }

    @Override
    public void display(GLAutoDrawable gldrawable) {
        GL2 gl = gldrawable.getGL().getGL2();
        gl.glClear(GL2.GL_COLOR_BUFFER_BIT);
        gl.glClear(GL2.GL_DEPTH_BUFFER_BIT);
        this.Ejes(gl);
        this.pintarSplines(gl);
        gl.glFlush();
    }

    @Override
    public void dispose(GLAutoDrawable gldrawable) {
    }
    
    //codigo
    @SuppressWarnings("static-access")
    private void Ejes(GL2 gl) {
        gl.glLineWidth(2);// width of line
        gl.glBegin(GL2.GL_LINES);
        //eje x
        gl.glColor3f(1, 0, 0);
        gl.glVertex3i(0, 0, 0);
        gl.glVertex3f(7.8f, 0, 0);
        //eje y
        gl.glColor3f(0, 1, 0);
        gl.glVertex3i(0, 0, 0);
        gl.glVertex3f(0, 7.7f, 0);
        //eje z
        gl.glColor3f(0, 0, 1);
        gl.glVertex3i(0, 0, 0);
        gl.glVertex3f(0, 0, 8.3f);
        gl.glEnd();
        gl.glColor3f(1, 0, 0);
        gl.glRasterPos3f(7.9f, 0, 0);
        glut.glutBitmapCharacter(glut.BITMAP_TIMES_ROMAN_24, 'x');
        gl.glColor3f(0, 1, 0);
        gl.glRasterPos3f(0, 7.9f, 0);
        glut.glutBitmapCharacter(glut.BITMAP_TIMES_ROMAN_24, 'y');
        gl.glColor3f(0, 0, 1);
        gl.glRasterPos3f(0, 0, 8.6f);
        glut.glutBitmapCharacter(glut.BITMAP_TIMES_ROMAN_24, 'z');
        gl.glLineWidth(1.5f);
    }

    private void pintarSplines(GL2 gl) {
        if (splines.size() < 2 || !activacion) {
            return;
        }
        
        gl.glPointSize(5.f);
        gl.glBegin(GL2.GL_POINTS);
        gl.glColor3f(.5f, .5f, 0);
        for (int i = 0; i < splines.size(); i += 3) {
            float[] vx = splines.get(i);
            float[] vy = splines.get(i + 1);
            float[] vz = splines.get(i + 2);
            for (float u = 0; u <= 1; u += 0.01) {
                gl.glVertex3f(vx[0] * u * u * u + vx[1] * u * u + vx[2] * u + vx[3],
                        vy[0] * u * u * u + vy[1] * u * u + vy[2] * u + vy[3],
                        vz[0] * u * u * u + vz[1] * u * u + vz[2] * u + vz[3]);
            }
        }
        gl.glEnd();
        this.splinesString(gl);
    }

    @SuppressWarnings("static-access")
    private void splinesString(GL2 gl) {
        String[] hts = hermite.toStringSplines();
        float posF = 10f;
        gl.glColor3f(1, 1, 1);
        for (int i = 0; i < hermite.numCoordSplines(); i++) {
            gl.glRasterPos2f(4.5f, posF);
            glut.glutBitmapString(GLUT.BITMAP_HELVETICA_12, hts[i]);
            if ((i + 1) % 3 == 0) {
                posF -= 0.5;
                gl.glRasterPos2f(4.5f, posF);
                glut.glutBitmapString(GLUT.BITMAP_HELVETICA_12, "");
            }
            posF -= 0.5;
        }
    }

    private void Formar() {
        // calculamos los splines formados
        if (splines.isEmpty()) {
            splines = hermite.calcularSplines();
            repaint();
        } else if ((splines.size() / 3) < hermite.size()) {
            splines.clear();
            hermite.clearSplines();
            splines = hermite.calcularSplines();
            repaint();
        }
    }

    public JPanel showPanel() {
        JPanel panel = new JPanel();
        this.initComponents(panel);
        return panel;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">
    private void initComponents(JPanel panel) {

        TFpux = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        TFpex = new javax.swing.JTextField();
        TFpuy = new javax.swing.JTextField();
        TFpey = new javax.swing.JTextField();
        TFpuz = new javax.swing.JTextField();
        TFpez = new javax.swing.JTextField();
        Bañadir = new javax.swing.JButton();
        Bgraficar = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        TApu_pe = new javax.swing.JTextArea();
        Bclear = new javax.swing.JButton();

        jLabel1.setText("X");

        jLabel2.setText("Y");

        jLabel3.setText("Z");

        jLabel4.setText("  Puntos / Pendientes");

        Bañadir.setText("Añadir");
        Bañadir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BañadirActionPerformed(evt);
            }
        });

        Bgraficar.setText("Graficar");
        Bgraficar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BgraficarActionPerformed(evt);
            }
        });

        jLabel5.setText("   Pun. y Pen Actuales");

        TApu_pe.setColumns(20);
        TApu_pe.setRows(5);
        jScrollPane1.setViewportView(TApu_pe);

        Bclear.setText("Clear");
        Bclear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BclearActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(panel);
        panel.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, 113, Short.MAX_VALUE)
                                                .addContainerGap())
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 113, Short.MAX_VALUE)
                                                .addContainerGap())
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 113, Short.MAX_VALUE)
                                                .addContainerGap())
                                        .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                                                .addComponent(jLabel1)
                                                                .addGap(18, 18, 18)
                                                                .addComponent(TFpux, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                                .addComponent(TFpex, javax.swing.GroupLayout.DEFAULT_SIZE, 39, Short.MAX_VALUE))
                                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                                                .addComponent(jLabel2)
                                                                .addGap(18, 18, 18)
                                                                .addComponent(TFpuy, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                                .addComponent(TFpey, javax.swing.GroupLayout.DEFAULT_SIZE, 39, Short.MAX_VALUE))
                                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                                                .addComponent(jLabel3)
                                                                .addGap(18, 18, 18)
                                                                .addComponent(TFpuz, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                                .addComponent(TFpez, javax.swing.GroupLayout.DEFAULT_SIZE, 39, Short.MAX_VALUE)))
                                                .addContainerGap())
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                        .addComponent(Bañadir, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 83, Short.MAX_VALUE)
                                                        .addComponent(Bgraficar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(Bclear, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 83, Short.MAX_VALUE))
                                                .addGap(24, 24, 24))))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(TFpux, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel1)
                                        .addComponent(TFpex, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(32, 32, 32)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel2)
                                        .addComponent(TFpuy, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(TFpey, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(32, 32, 32)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel3)
                                        .addComponent(TFpuz, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(TFpez, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(35, 35, 35)
                                .addComponent(Bañadir)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(Bgraficar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(Bclear)
                                .addGap(14, 14, 14)
                                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 142, Short.MAX_VALUE)
                                .addContainerGap())
        );
    }// </editor-fold>

    private void BañadirActionPerformed(java.awt.event.ActionEvent evt) {
        String[] pp = {TFpux.getText(), TFpuy.getText(), TFpuz.getText(),
            TFpex.getText(), TFpey.getText(), TFpez.getText()};
        if (pp[0].length() > 0 && pp[1].length() > 0
                && pp[2].length() > 0 && pp[3].length() > 0
                && pp[4].length() > 0 && pp[5].length() > 0) {
            Point3D pu3d = new Point3D(Float.parseFloat(pp[0]),
                    Float.parseFloat(pp[1]),
                    Float.parseFloat(pp[2]));
            Point3D pe3d = new Point3D(Float.parseFloat(pp[3]),
                    Float.parseFloat(pp[4]),
                    Float.parseFloat(pp[5]));
            TApu_pe.append("\n" + pu3d.toString() + "; " + pe3d.toString() + "\n");
            hermite.add(pu3d, pe3d);
        }
    }

    private void BgraficarActionPerformed(java.awt.event.ActionEvent evt) {
        if (hermite.size() < 2) {
            return;
        }
        activacion = true;
        this.Formar();
    }

    private void BclearActionPerformed(java.awt.event.ActionEvent evt) {
        hermite.clearAll();
        splines.clear();
        TApu_pe.setText("");
        activacion = false;
        repaint();
    }
}
