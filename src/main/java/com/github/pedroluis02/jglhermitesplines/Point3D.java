package com.github.pedroluis02.jglhermitesplines;

/**
 * Point 3D
 * @author Pedro Luis
 */
public class Point3D {

    private float x, y, z;

    public Point3D() {
        this(0, 0, 0);
    }

    public Point3D(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Point3D(float[] p3d) {
        this(p3d[0], p3d[1], p3d[2]);
    }

    public Point3D(Point3D p3d) {
        this(p3d.x, p3d.y, p3d.z);
    }

    public void setPoint3D(Point3D p3d) {
        x = p3d.x;
        y = p3d.y;
        z = p3d.z;
    }

    public void setPoint3D(float[] p3d) {
        x = p3d[0];
        y = p3d[1];
        z = p3d[2];
    }

    public Point3D getPoint3D() {
        return new Point3D(x, y, z);
    }

    public float[] getPoint3Dv() {
        float[] point3d = new float[3];
        point3d[0] = x;
        point3d[1] = y;
        point3d[2] = z;
        return point3d;
    }

    public void move(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }

    public void setZ(float z) {
        this.z = z;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getZ() {
        return z;
    }

    @Override
    public String toString() {
        return "[" + x + ", " + y + ", " + z + "]";
    }
}
