package com.github.pedroluis02.jglhermitesplines;

import java.util.ArrayList;

/**
 * Hermite
 * @author Pedro Luis
 */
public class Hermite {

    private int size;
    private ArrayList <Point3D> puntos;
    private ArrayList <Point3D> pendientes;
    private ArrayList <float[]> splines;
    private float[][] matrixHermite = new float[][]{
        {2, -2, 1, 1},
        {-3, 3, -2, -1},
        {0, 0, 1, 0},
        {1, 0, 0, 0}
    };

    public Hermite() {
        this.size = 0;
        this.puntos = new ArrayList<Point3D>();
        this.pendientes = new ArrayList<Point3D>();
        this.splines = new ArrayList<float[]>();
    }

    public void add(Point3D pu, Point3D pe) {
        this.puntos.add(pu);
        this.pendientes.add(pe);
        this.size++;
    }

    public Point3D getPunto(int pos) {
        return puntos.get(pos);
    }

    public Point3D getPendiente(int pos) {
        return pendientes.get(pos);
    }

    public float[] getSpline(int pos) {
        return splines.get(pos);
    }

    public int size() {
        return this.size;
    }

    public int numCoordSplines() {
        return 3 * (size - 1);
    }

    public void clearAll() {
        this.size = 0;
        this.puntos.clear();
        this.pendientes.clear();
        this.splines.clear();
    }

    public void clearSplines() {
        this.splines.clear();
    }

    public String[] toStringSplines() {
        String[] pp = new String[this.numCoordSplines()];
        int j = 0;
        for (int i = 0; i < this.numCoordSplines(); i += 3) {
            float[] vx = splines.get(i);
            float[] vy = splines.get(i + 1);
            float[] vz = splines.get(i + 2);
            pp[i] = "vx" + j + "[" + vx[0] + ", " + vx[1] + ", " + vx[2] + ", " + vx[3] + "]";
            pp[i + 1] = "vy" + j + "[" + vy[0] + ", " + vy[1] + ", " + vy[2] + ", " + vy[3] + "]";
            pp[i + 2] = "vz" + j + "[" + vz[0] + ", " + vz[1] + ", " + vz[2] + ", " + vz[3] + "]";
            j++;
        }
        return pp;
    }

    public float[] multMatrix_Vector(float[] v) {
        int k = -1;
        float[] R = new float[4];
        for (int i = 0; i < 4; i++) {
            float aux = 0;
            for (int j = 0; j < 4; j++) {
                aux += matrixHermite[i][j] * v[j];
            }
            R[++k] = aux;
        }
        return R;
    }

    public ArrayList<float[]> calcularSplines() {
        for (int i = 0; i < puntos.size() - 1; i++) {
            Point3D pu3d1 = this.getPunto(i);
            Point3D pe3d1 = this.getPendiente(i);

            Point3D pu3d2 = this.getPunto(i + 1);
            Point3D pe3d2 = this.getPendiente(i + 1);

            float[] vx = new float[]{pu3d1.getX(), pu3d2.getX(), pe3d1.getX(), pe3d2.getX()};
            float[] vy = new float[]{pu3d1.getY(), pu3d2.getY(), pe3d1.getY(), pe3d2.getY()};
            float[] vz = new float[]{pu3d1.getZ(), pu3d2.getZ(), pe3d1.getZ(), pe3d2.getZ()};

            splines.add(multMatrix_Vector(vx));
            splines.add(multMatrix_Vector(vy));
            splines.add(multMatrix_Vector(vz));
        }
        return splines;
    }
}
